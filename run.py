#!flask/bin/python
import logging
import sys
from app import app

# Logging configuration
logging.basicConfig(format='%(asctime)s - %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='events.log', level=logging.DEBUG)
logger = logging.getLogger()
stdout = logging.StreamHandler(sys.stdout)
stdout.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
stdout.setFormatter(formatter)
logger.addHandler(stdout)

logging.info('###############-- RSYSTEM STARTING --###############')
app.run(debug=True)
