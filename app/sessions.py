from datetime import datetime

from .database import repository

repo = repository.Repository()


def validate_session(unitid):
    if unitid is 0:
        return False
    else:
        now = datetime.now()
        session = repo.get_session(unitid)

        currenttime = (str(now.hour) + ":" + str(now.minute) + ":" + str(now.second))

        if session.Unit_Weekday != datetime.today().weekday():
            return False

        sessiontime = str(session.Unit_StartTime)
        format = '%H:%M:%S'

        timedelta = datetime.strptime(sessiontime, format) - datetime.strptime(currenttime, format)

        if timedelta.total_seconds() > 0:
            return False

        sessiontime = str(session.Unit_FinishTime)

        timedelta = datetime.strptime(sessiontime, format) - datetime.strptime(currenttime, format)

        if timedelta.total_seconds() < 0:
            return False

        return True


def autofill_attendance(unitid):
    if unitid is 0:
        return False
    else:
        students = repo.get_unit_student(unitid)
        attended = repo.get_attendees(unitid)
    for student in students[:]:
        if student in attended:
            students.remove(student)
    for student in students[:]:
        repo.set_absent(student, unitid)
