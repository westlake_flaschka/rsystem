from flask import redirect, url_for, flash
from flask_login import current_user
from functools import wraps


def requires_login(failure='login', valid_user_types=tuple()):
    def decorator(f):
        @wraps(f)
        def check(*args, **kwargs):
            user = current_user

            # First check if logged in with an authenticated Account
            if not user.is_authenticated and user.is_anonymous:
                flash('Must be logged in to view this page')
                return redirect(url_for(failure))

            # If any logged in user can make request
            elif len(valid_user_types) is 0:
                return f(*args, **kwargs)

            # Finally if the user is permitted to access this page
            elif current_user.accountPermission in valid_user_types:
                return f(*args, **kwargs)

            # Default to failure
            flash('Error: Not authorized to view this page')
            return redirect(url_for(failure))

        return check

    return decorator
