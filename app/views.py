from flask import render_template, flash, redirect, session, url_for, request, g, make_response
from flask_login import login_user, logout_user, current_user, login_required
from app import *
from .forms import LoginForm, RegisterForm
import bcrypt
from .auth import *
from .errors import error_handler
from .pages import *
from .api import *

from .database import repository
from .exceptions import *

repo = repository.Repository()

renderer = PageRenderer()


@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
@error_handler()
@requires_login(failure='login')
def index():
    user_pages = {
        'A': renderer.admin,
        'L': renderer.lecturer,
        'S': renderer.student
    }

    if current_user.accountPermission not in user_pages:
        print('Invalid permission in user %s' % current_user.accountID)
        print('Logging out user %s' % current_user.accountID)
        flash('Your account does not have the correct permissions. Please contact an administrator')
        logout_user()
        return redirect('login')

    target_page = user_pages[current_user.accountPermission]
    return target_page()


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        try:
            user = repo.get_account(email=form.email.data)
        except AccountNotFoundException as e:
            logging.warning('User tried to login with incorrect email: %s' % form.email.data)
            flash('User with this email does not exist')
            return redirect(url_for('login'))

        session['remember_me'] = form.remember_me.data

        password_valid = bcrypt.checkpw(form.password.data.encode('utf-8'), user.accountPassword.encode('utf-8'))

        if password_valid:
            login_user(user, remember=form.remember_me.data)
            logging.info('Logged in user: %s' % form.email.data)
            flash('You have been logged in')
            return redirect(url_for('index'))
        else:
            logging.warning('User attempted logging in with incorrect password, Account email: %s' % user.accountEmail)
            flash('Account details incorrect')

    return render_template('login.html',
                           title='Sign In',
                           form=form,
                           hideNav=True)


@app.route('/logout', methods=['GET'])
@requires_login(failure='login')
def logout():
    # Catch all errors for now
    try:
        logout_user()
        flash('You have been logged out successfully')
    except Exception as e:
        flash('Logout failed: %s' % e)

    return redirect(url_for('login'))


# This will be removed in time, all accounts should be created by an admin
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()

    # If form is valid and submitted
    if form.validate_on_submit():
        try:
            repo.create_account(form.email.data, form.password.data, 'A')
            flash('Account created')
            return redirect(url_for('index'))
        except Exception as e:
            flash('Creation failed: %s' % e)
            return redirect(url_for('register'))

    # Failed or not attempted yet
    return render_template('register.html',
                           title='Register',
                           user=current_user,
                           form=form)


@lm.user_loader
def load_user(uid):
    try:
        user = repo.get_account(account_id=uid)
        logging.info('Fetched Account, with ID %s' % uid)
        return user
    except Exception:
        msg = 'Failed to find user with id: %s, in user_loader' % uid
        logging.error(msg)
        flash(msg)
        session.clear()
        return None
