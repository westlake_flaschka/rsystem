window.addEventListener('load', displayAll);

function displayAll() {
    var messages = $('meta[name=msg]');
    var navbar = $('#navbar');

    for (var msg of messages) {
        var text = $(msg).attr("content");
        $.notify(text, 'error');
    }
}