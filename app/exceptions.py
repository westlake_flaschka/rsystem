class AccountNotFoundException(Exception):
    pass


class AccountDetailsNotSuppliedException(Exception):
    pass
