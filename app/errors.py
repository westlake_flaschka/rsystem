from functools import wraps
import traceback
from app import *
from flask import render_template


def error_handler():
    def decorator(f):
        @wraps(f)
        def handle(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                if app.debug:
                    raise e
                else:
                    error = {
                        'name': 'Generic',
                        'data': traceback.format_exc()
                    }
                    return render_template('error.html', error=error)

        return handle

    return decorator
