from flask import Flask
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config')

lm = LoginManager()
lm.init_app(app)

# app refers to app package not variable
from app import app
from app import views

