from flask import render_template, flash, redirect, session, url_for, request, g, make_response
import logging
from .pages import *
from .database import repository

repo = repository.Repository()


@app.route('/create_account', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_account():
    form = AdminCreateUser()

    if form.validate_on_submit():
        try:
            inserted = repo.create_account(form.email.data, form.password.data, form.account_permission.data)
            logging.info('Account created with ID: %s' % inserted)
            flash('Success')
        except Exception as e:
            msg = 'Failed to insert Account: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(url_for('index'))


@app.route('/create_student', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_student():
    form = AdminCreateStudent()

    if form.validate_on_submit():
        # Account fields
        email = form.email.data
        password = form.password.data

        # Student fields
        fname = form.fname.data
        lname = form.lname.data
        phone = form.phone.data
        tutor = form.tutor.data
        year = form.year.data

        try:
            stud_id = repo.add_student(fname, lname, phone, tutor, year)
            logging.info('Created Student with ID: %s' % stud_id)
            account_id = repo.create_account(email, password, 'S', stud_id=stud_id)
            logging.info('Created Account with ID: %s' % account_id)
            flash('Success')
        except Exception as e:
            msg = 'Failed to create Student: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(url_for('index'))


@app.route('/create_lecturer', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_lecturer():
    form = AdminCreateLecturer()

    if form.validate_on_submit():
        # Account options
        email = form.email.data
        password = form.password.data
        permission = 'L'

        # Lecturer specific
        fname = form.fname.data
        lname = form.lname.data
        phone = form.phone.data
        dept = form.dept.data

        try:
            lect_id = repo.add_lecturer(fname, lname, phone, dept)
            logging.info('Created Lecturer with ID: %s' % lect_id)
            account_id = repo.create_account(email, password, permission, lect_id=lect_id)
            logging.info('Created Account with ID: %s' % account_id)
            flash('Success')
        except Exception as e:
            msg = 'Failed to create Lecturer: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(url_for('index'))


@app.route('/create_course', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_course():
    form = AdminCreateCourse()

    if form.validate_on_submit():
        name = form.name.data

        try:
            inserted = repo.create_course(name)
            logging.info('Created Course with ID: %s, Name: %s' % (inserted, name))
            flash('Success')
        except Exception as e:
            msg = 'Failed to add new Course: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(url_for('index'))


@app.route('/create_course_info', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_course_info():
    course_info_form = AdminCreateCourseInformation()

    if course_info_form.validate_on_submit():
        year = course_info_form.year.data
        course = course_info_form.course.data

        try:
            inserted = repo.create_course_information(year, course)
            logging.info('Created Course_Information with ID: %s' % inserted)
            flash('Success')
        except Exception as e:
            msg = 'Failed  to add Course_Information: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(request.referrer)


@app.route('/create_unit', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_unit():
    create_unit_form = AdminCreateUnit()

    if create_unit_form.validate_on_submit():
        name = create_unit_form.name.data
        unit_credits = create_unit_form.credits.data
        coordinator = create_unit_form.coordinator.data

        try:
            inserted = repo.add_unit(name, unit_credits, coordinator)
            logging.info('Created Unit with ID: %s' % inserted)
            flash('Success')
        except Exception as e:
            msg = 'Failed to add Unit: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(request.referrer)


@app.route('/create_unit_information', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def create_unit_info():
    form = AdminCreateUnitInformation()

    if form.validate_on_submit():
        room = form.room.data
        weekday = form.weekday.data
        start = form.start.data
        end = form.end.data
        lecturer = form.lecturer.data
        unit = form.unit.data

        try:
            inserted = repo.create_unit_information(room, weekday, start, end, lecturer, unit)
            logging.info('Created Unit_Information with ID: %s' % inserted)
        except Exception as e:
            msg = 'Failed to add Unit_Information: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(request.referrer)


@app.route('/link_unit', methods=['POST'])
@requires_login(failure='login', valid_user_types=['A'])
def link_unit():
    form = LinkUnit()

    if form.validate_on_submit():
        unit = form.unit.data
        course_year = form.year.data

        try:
            repo.create_course_unit_link(course_year, unit)
        except Exception as e:
            msg = 'Failed to create link: %s' % e
            logging.error(msg)
            flash(msg)

    return redirect(request.referrer)
