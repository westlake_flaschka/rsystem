from flask import render_template, flash, redirect, session, url_for, request, g, make_response
from flask_login import login_user, logout_user, current_user, login_required
from app import *
from .forms import *
from .auth import *


class PageRenderer:
    @staticmethod
    def admin(**kwargs):
        title = kwargs['title'] if 'title' in kwargs else 'Home'

        create_form = AdminCreateUser()
        create_student = AdminCreateStudent()
        create_lecturer = AdminCreateLecturer()
        create_course = AdminCreateCourse()
        create_course_info = AdminCreateCourseInformation()
        create_unit = AdminCreateUnit()
        create_unit_information = AdminCreateUnitInformation()
        link_unit = LinkUnit()

        return render_template('admin.html',
                               title=title,
                               user=current_user,
                               account_form=create_form,
                               create_student=create_student,
                               create_lecturer=create_lecturer,
                               create_course=create_course,
                               create_course_info=create_course_info,
                               create_unit=create_unit,
                               create_unit_information=create_unit_information,
                               link_unit=link_unit)

    @staticmethod
    def student(**kwargs):
        title = kwargs['title'] if 'title' in kwargs else 'Home'

        student_course_year = repo.get_student(email=current_user.accountEmail).Course

        sessions = repo.get_sessions_for_course_information(student_course_year)
        pretty_sessions = []
        for i in range(len(sessions)):
            pretty_sessions.append(list(sessions[i]))
            pretty_sessions[i][3] = str(pretty_sessions[i][3])
            pretty_sessions[i][4] = str(pretty_sessions[i][4])
            pretty_sessions[i][2] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'][pretty_sessions[i][2]]

        return render_template('student.html',
                               title=title,
                               user=current_user,
                               sessions=pretty_sessions)

    @staticmethod
    def lecturer(**kwargs):
        title = kwargs['title'] if 'title' in kwargs else 'Home'

        sessions = repo.get_all_unit_information()
        pretty_sessions = []
        for i in range(len(sessions)):
            pretty_sessions.append(list(sessions[i]))
            pretty_sessions[i][3] = str(pretty_sessions[i][3])
            pretty_sessions[i][4] = str(pretty_sessions[i][4])
            pretty_sessions[i][2] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'][pretty_sessions[i][2]]

        return render_template('lecturer.html',
                               title=title,
                               user=current_user,
                               sessions=pretty_sessions)
