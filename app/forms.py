from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import DataRequired, Email, Length
from .database import repository

repo = repository.Repository()

days = [(0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'), (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')]


def make_unit_choices():
    all_units = repo.get_all_units()
    return [(u[0], u[1]) for u in all_units]

def make_lecturer_choices():
    all_lecturers = repo.get_lecturers()
    return [(l.Lect_ID, l.LName + " " + l.Fname + " - " + l.Dept) for l in all_lecturers]


class LoginForm(FlaskForm):
    email = StringField('username', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8)])
    remember_me = BooleanField('remember_me', default=False)


class RegisterForm(FlaskForm):
    email = StringField('username', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8)])


class AdminCreateUser(FlaskForm):
    email = StringField('username', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8)])
    account_permission = SelectField('account_permission', choices=[('A', 'Admin'), ('S', 'Student'), ('L', 'Lecturer')]
                                     , validators=[DataRequired()])


class AdminCreateStudent(FlaskForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Might be a bad idea doing this here, may cause lots of calls to database
        # However choices must be set for each instance of class or app will fail when validating this form
        self.tutor.choices = make_lecturer_choices()
        course_years = repo.get_all_course_info()
        self.year.choices = [(c[0], str(c[1]) + " - Year: " + str(c[2])) for c in course_years]

    # Account Fields
    email = StringField('username', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8)])

    # Student fields
    fname = StringField('fname', validators=[DataRequired(), Length(max=20)])
    lname = StringField('lname', validators=[DataRequired(), Length(max=20)])
    phone = StringField('phone', validators=[DataRequired(), Length(max=15)])
    year = SelectField('year', validators=[DataRequired()], coerce=int)

    # Options are fetched on page view
    tutor = SelectField('tutor', validators=[DataRequired()], coerce=int)


class AdminCreateLecturer(FlaskForm):
    # Account fields
    email = StringField('username', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8)])

    # Lecturer Fields
    fname = StringField('fname', validators=[DataRequired(), Length(max=20)])
    lname = StringField('lname', validators=[DataRequired(), Length(max=20)])
    phone = StringField('phone', validators=[DataRequired(), Length(max=15)])
    dept = StringField('dept', validators=[DataRequired(), Length(max=45)])


class AdminCreateCourse(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(max=30)])


class AdminCreateCourseInformation(FlaskForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        courses = repo.get_all_courses()
        self.course.choices = courses

    year = SelectField('year', choices=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4')], validators=[DataRequired()])
    course = SelectField('course', validators=[DataRequired()], coerce=int)


class AdminCreateUnit(FlaskForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.coordinator.choices = make_lecturer_choices()

    name = StringField('name', validators=[DataRequired(), Length(max=20)])
    credits = IntegerField('credits', validators=[DataRequired()])
    coordinator = SelectField('coordinator', coerce=int)


class AdminCreateUnitInformation(FlaskForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.lecturer.choices = make_lecturer_choices()
        self.unit.choices = make_unit_choices()

    room = StringField('room', validators=[DataRequired(), Length(max=10)])
    weekday = SelectField('weekday', choices=days, coerce=int)
    start = DateTimeField('start', validators=[DataRequired()], format='%H:%M')
    end = DateTimeField('end', validators=[DataRequired()], format='%H:%M')
    lecturer = SelectField('lecturer', coerce=int)
    unit = SelectField('unit', coerce=int)


class LinkUnit(FlaskForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.unit.choices = make_unit_choices()
        course_years = repo.get_all_course_info()
        self.year.choices = [(c[0], str(c[1]) + " - Year: " + str(c[2])) for c in course_years]

    year = SelectField('year', validators=[DataRequired()], coerce=int)
    unit = SelectField('unit', coerce=int)
