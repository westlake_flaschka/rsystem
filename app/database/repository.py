from .entities import *
import bcrypt
import mysql.connector
from ..exceptions import *


# All database operations go through this class
class Repository(object):
    def __init__(self):
        option_file_name = 'mysql.cnf'

        try:
            self.cnx = mysql.connector.connect(option_files=option_file_name, option_groups='admin')
            self.cnx.autocommit = True
        except (mysql.connector.errors.ProgrammingError, ValueError) as e:
            import os
            cwd = os.getcwd()
            desired_location = os.path.join(cwd, option_file_name)

            print('Could not connect to MySQL. Please provide a valid connection file at %s' % desired_location)
            print(e)

            # Critical failure, shutdown server
            exit(1)

    # Retrieve Account
    def get_account(self, account_id=0, email=''):
        if account_id is 0:
            cursor = self.cnx.cursor()
            query = "SELECT * FROM Account WHERE accountEmail = %s"

            # Must be wrapped in list for mysqlconnector
            email = [email]

            cursor.execute(query, email)

            retrieved = cursor.fetchone()

            if retrieved is None:
                cursor.close()
                raise AccountNotFoundException('Could not find Account with Email %s' % email)

            user = Account(retrieved[0], retrieved[1], retrieved[2], retrieved[3], stud_id=retrieved[4], lect_id=retrieved[5])

            cursor.close()
            return user

        elif not email:
            cursor = self.cnx.cursor()
            query = "SELECT * FROM Account WHERE accountID = %s"

            # Must be wrapped in list for mysqlconnector
            account_id = [account_id]

            cursor.execute(query, account_id)

            retrieved = cursor.fetchone()

            if retrieved is None:
                cursor.close()
                raise AccountNotFoundException('Could not find account with id %s' % account_id)

            user = Account(retrieved[0], retrieved[1], retrieved[2], retrieved[3])

            cursor.close()
            return user

        raise AccountDetailsNotSuppliedException('Neither account_id nor email set')

    # Create a NEW Account
    def create_account(self, email, password, permission, stud_id=None, lect_id=None):
        password = password.encode('utf-8')  # bcrypt needs encoded strings
        pw_hash = bcrypt.hashpw(password, bcrypt.gensalt())

        if permission not in ['A', 'L', 'S']:
            raise Exception('Invalid user type: %s' % permission)

        cursor = self.cnx.cursor()

        # If inserting lecturer
        if lect_id:
            if permission != 'L':
                cursor.close()
                raise Exception('Incorrect permission for Lecturer, is %s should be %s' % (permission,'L'))

            query = ("INSERT INTO Account "
                     "(accountEmail, accountPassword, accountPermission, Lect_ID)"
                     "VALUES (%s, %s, %s, %s)")
            values = (email, pw_hash, permission, lect_id)
        elif stud_id:
            if permission != 'S':
                cursor.close()
                raise Exception('Incorrect permission for Student, is %s should be %s' % (permission, 'S'))

            query = ("INSERT INTO Account "
                     "(accountEmail, accountPassword, accountPermission, Stud_ID)"
                     "VALUES (%s, %s, %s, %s)")
            values = (email, pw_hash, permission, stud_id)
        # Default to basic account
        else:
            query = ("INSERT INTO Account "
                     "(accountEmail, accountPassword, accountPermission)"
                     "VALUES (%s, %s, %s)")
            values = (email, pw_hash, permission)

        cursor.execute(query, values)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    # Update an existing account
    def update_account(self, account):
        pass

    # Retrieve Lecturer
    def get_lecturer(self, lect_id=0, email=''):
        # Search by email
        if lect_id is 0:
            cursor = self.cnx.cursor()

            # Join Account table and lecturer table
            query = "SELECT Lecturer.* from Lecturer JOIN Account ON Account.Lect_ID" \
                    "= Lecturer.Lect_ID WHERE Account.accountEmail = %s"

            # Must be wrapped in list for mysqlconnector
            email = [email]

            cursor.execute(query, email)

            retrieved = cursor.fetchone()

            if retrieved is None:
                cursor.close()
                raise Exception('Could not find lecturer with email %s' % email)

            lecturer = Lecturer(retrieved[0], retrieved[1], retrieved[2], retrieved[3], retrieved[4], email)

            cursor.close()
            return lecturer

        # Search by ID
        elif not email:
            cursor = self.cnx.cursor()

            query = "SELECT Lecturer.*, Account.accountEmail from Lecturer JOIN Account ON " \
                    "Account.Lect_ID = Lecturer.Lect_ID WHERE Account.Lect_ID = %s;"

            lect_id = [lect_id]

            cursor.execute(query, lect_id)

            retrieved = cursor.fetchone()

            if retrieved is None:
                cursor.close()
                raise Exception('Could not find lecturer with id %s' % lect_id)

            lecturer = Lecturer(retrieved[0], retrieved[1], retrieved[2], retrieved[3], retrieved[4], retrieved[5])

            cursor.close()
            return lecturer

        raise Exception('Neither lect_id nor email set')

    # Fetch all lecturers
    def get_lecturers(self):
        cursor = self.cnx.cursor()
        query = "SELECT Lecturer.*, Account.accountEmail from Lecturer JOIN Account ON Account.Lect_ID = Lecturer.Lect_ID"
        cursor.execute(query)
        retrieved = cursor.fetchall()
        cursor.close()

        lecturers = []

        for result in retrieved:
            lecturers.append(Lecturer(result[0], result[1], result[2], result[3], result[4], result[5]))

        return lecturers

    # get student
    def get_student(self, stud_id=0, email=''):
        # search by email
        if stud_id is 0:
            cursor = self.cnx.cursor()

            # Join Account table and lecturer table
            query = "SELECT Student.* from Student JOIN Account ON Account.Stud_ID" \
                    "= Student.Stud_ID WHERE Account.accountEmail = %s"

            # Must be wrapped in list for mysqlconnector
            email = [email]

            cursor.execute(query, email)

            retrieved = cursor.fetchone()

            if retrieved is None:
                cursor.close()
                raise Exception('Could not find student with email %s' % email)

            student = Student(retrieved[0], retrieved[1], retrieved[2], retrieved[3], retrieved[4], retrieved[5], email)

            cursor.close()
            return student

        # Search by ID
        elif not email:
            cursor = self.cnx.cursor()

            query = "SELECT Student.*, account.accountEmail from Student JOIN account ON " \
                    "Account.Stud_ID = Student.Stud_ID WHERE Account.Stud_ID = %s;"

            stud_id = [stud_id]

            cursor.execute(query, stud_id)

            retrieved = cursor.fetchone()

            if retrieved is None:
                cursor.close()
                raise Exception('Could not find student with id %s' % stud_id)

            student = Student(retrieved[0], retrieved[1], retrieved[2], retrieved[3],
                              retrieved[4], retrieved[5], retrieved[6])

            cursor.close()
            return student

        raise Exception('Neither stud_id nor email set')

    # Valid session
    def get_session(self, unit_id):
        if unit_id is 0:
            raise Exception('unit_id not set to valid value, is %s' % unit_id)

        cursor = self.cnx.cursor()
        query = "SELECT * FROM unit_information WHERE Unit_ID = %s"
        unit_id = [unit_id]

        cursor.execute(query, unit_id)

        retrieved = cursor.fetchone()

        if retrieved is None:
            cursor.close()
            raise Exception('Could not session with id %s' % unit_id)

        session = UnitInfo(retrieved[0], retrieved[1], retrieved[2], retrieved[3], retrieved[4], retrieved[5],
                           retrieved[6])

        cursor.close()
        return session

    def create_course(self, coursename):
        cursor = self.cnx.cursor()
        query = ("INSERT INTO Course (Course_Name) VALUES(%s)")

        coursename = [coursename]

        cursor.execute(query, coursename)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    def get_all_courses(self):
        cursor = self.cnx.cursor()
        query = "SELECT * FROM Course"

        cursor.execute(query)

        retrieved = cursor.fetchall()
        cursor.close()
        return retrieved

    def create_course_information(self, year, course_id):
        cursor = self.cnx.cursor()

        data = (year, course_id)

        queryCheck = "SELECT * FROM Course_Information WHERE Course_Year = %s AND Course_ID = %s"
        cursor.execute(queryCheck, data)
        result = cursor.fetchall()

        if len(result) > 0:
            self.cnx.commit()
            cursor.close()
            raise Exception('This year already exists in Course_Information')


        query = "INSERT INTO Course_Information (Course_Year, Course_ID) VALUES (%s, %s)"

        cursor.execute(query, data)
        cursor.close()
        self.cnx.commit()
        return cursor.lastrowid

    def create_unit_information(self, room, weekday, start, end, lect_id, unit_id):
        cursor = self.cnx.cursor()

        query = "INSERT INTO Unit_Information (Unit_Room, Unit_Weekday, Unit_StartTime, Unit_FinishTime, Unit_Lecturer, Units_ID)" \
                "VALUES (%s, %s, %s, %s, %s, %s)"

        data = (room, weekday, start, end, lect_id, unit_id)

        cursor.execute(query, data)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    def add_student(self, fname, lname, phone, tutor, year):
        cursor = self.cnx.cursor()

        query = ("INSERT INTO Student "
                 "(Fname, Lname, Phone, Tutor, Course_Year)"
                 "VALUES (%s, %s, %s, %s, %s)")
        values = (fname, lname, phone, tutor, year)

        cursor.execute(query, values)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    def add_lecturer(self, fname, lname, phone, dept):
        cursor = self.cnx.cursor()

        query = ("INSERT INTO Lecturer (Fname, Lname, Phone, Dept) VALUES (%s, %s, %s, %s)")

        values = (fname, lname, phone, dept)

        cursor.execute(query, values)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    def add_unit(self, name, credit, coord):
        cursor = self.cnx.cursor()

        query = "INSERT INTO Unit (Unit_Name, Unit_Credit, Unit_Coord) VALUES (%s, %s, %s)"

        values = (name, credit, coord)

        cursor.execute(query, values)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    def get_all_units(self):
        cursor = self.cnx.cursor()
        query = "SELECT * FROM Unit"
        cursor.execute(query)
        retrieved = cursor.fetchall()
        cursor.close()
        return retrieved

    # Fetch attendance based on a student's name
    def get_attendance_by_name(self, fname, lname):
        cursor = self.cnx.cursor()

        query = ("Select unit.Unit_Name, SUM(IF(Attendended = 1, 1,0)) as `Attended`, SUM(IF(Attendended = 0,1,0)) as `NotAttended`,"
            "COUNT(attendance.Attendended) AS `TotalLessons`, student.Stud_ID,student.Fname, student.Lname FROM attendance"
            "JOIN unit_assignment on unit_assignment.Unit_AssignmentID = attendance.Unit_Student"
            "JOIN student on unit_assignment.Student_ID = student.Stud_ID"
            "JOIN unit_information on unit_assignment.Unit_ID = unit_information.Unit_ID"
            "JOIN unit on unit.Unit_ID = unit_information.Units_ID"
            "WHERE student.Fname = %s AND student.Lname = %s"
            "GROUP BY unit.Unit_Name")

        data = [fname, lname]
        cursor.execute(query, data)
        retrieved = cursor.fetchall()

        cursor.close()
        return retrieved

    def get_attendance_percentage(self, stud_id=0):
        cursor = self.cnx.cursor()

        query = ("SELECT CAST("
                   "(SELECT SUM(IF(Attendended = 1, 1,0)) FROM attendance"
                       " JOIN unit_assignment on unit_assignment.Unit_AssignmentID = attendance.Unit_Student"
                        "JOIN student on unit_assignment.Student_ID = student.Stud_ID"
                    "WHERE student.Stud_ID = 1)/"
                    "(SELECT COUNT(attendance.Attendended) FROM attendance"
                        "JOIN unit_assignment on unit_assignment.Unit_AssignmentID = attendance.Unit_Student"
                        "JOIN student on unit_assignment.Student_ID = student.Stud_ID"
                    "WHERE student.Stud_ID = %s)*100"
                "as DECIMAL(10,2))"
                "AS `Attendance Percentage`")

        stud_id = [stud_id]
        cursor.execute(query, stud_id)
        retrieved = cursor.fetchone()

        cursor.close()
        return retrieved

    # Get student attendance for all units or for a specific unit
    def get_attendance(self, stud_id=0, unit_id=0):
        cursor = self.cnx.cursor()

        if unit_id is 0:
            query = (
            "Select unit.Unit_Name, SUM(IF(Attendended = 1, 1,0)) as `Attended`, SUM(IF(Attendended = 0,1,0)) as `NotAttended`, "
            "COUNT(attendance.Attendended) AS `TotalLessons`, student.Stud_ID,student.Fname, student.Lname FROM attendance "
            "JOIN unit_assignment on unit_assignment.Unit_AssignmentID = attendance.Unit_Student "
            "JOIN student on unit_assignment.Student_ID = student.Stud_ID "
            "JOIN unit_information on unit_assignment.Unit_ID = unit_information.Unit_ID "
            "JOIN unit on unit.Unit_ID = unit_information.Units_ID "
            "WHERE student.Stud_ID = %s "
            "GROUP BY unit.Unit_Name")

            stud_id = [stud_id]
            cursor.execute(query, stud_id)
            retrieved = cursor.fetchall()

        else:
            # Get student attendance for a specific unit
            query = (
            "Select unit.Unit_Name, SUM(IF(Attendended = 1, 1,0)) as `Attended`, SUM(IF(Attendended = 0,1,0)) as `NotAttended`,"
            "COUNT(attendance.Attendended) AS `TotalLessons`, student.Stud_ID,student.Fname, student.Lname FROM attendance "
            "JOIN unit_assignment on unit_assignment.Unit_AssignmentID = attendance.Unit_Student "
            "JOIN student on unit_assignment.Student_ID = student.Stud_ID "
            "JOIN unit_information on unit_assignment.Unit_ID = unit_information.Unit_ID "
            "JOIN unit on unit.Unit_ID = unit_information.Units_ID "
            "WHERE student.Stud_ID = %s AND unit.Unit_ID = %s")

            data = [stud_id, unit_id]
            cursor.execute(query, data)
            retrieved = cursor.fetchall()

        cursor.close()
        return retrieved

    def mark_attendance(self, stud_id, unit_id, attended):
        if unit_id is 0:
            raise Exception('unit_id not set to valid value, is %s' % unit_id)
        if stud_id is 0:
            raise Exception('stud_id not set to valid value, is %s' % stud_id)
        if attended not in [True, False]:
            raise Exception('attended not set to boolean, is %s' % attended)

        cursor = self.cnx.cursor()
        query = "SELECT * FROM Unit_assignment WHERE Unit_ID = %s AND Student_ID = %s "

        values = (unit_id, stud_id[0])
        cursor.execute(query, values)
        retrieved = cursor.fetchone()

        if retrieved is None:
            cursor.close()
            raise Exception('No unit_assignments found for Unit_ID %s and Student_ID %s' % unit_id, stud_id)

        # Typo 'Attendended' in line with database at this time
        query = "INSERT INTO attendance (Unit_Student, Attendended, `date`) VALUES (%s, %s, NOW())"

        values = (retrieved[0], int(attended))

        cursor.execute(query, values)
        self.cnx.commit()
        cursor.close()
        return cursor.lastrowid

    # Get students which are attending a specific session
    def get_unit_student(self, unit_id):
        if unit_id is 0:
            raise Exception('No unit_id specified')

        cursor = self.cnx.cursor()
        query = "SELECT Student_ID from Unit_assignment WHERE Unit_ID = %s"

        unit_id = [unit_id]
        cursor.execute(query, unit_id)
        retrieved = cursor.fetchall()

        cursor.close()
        return retrieved

    def get_all_unit_information(self):
        cursor = self.cnx.cursor()
        query = ("SELECT Unit.Unit_Name, Unit_Room, Unit_Weekday, Unit_StartTime, Unit_FinishTime FROM Unit_Information "
                 "Join Unit ON Unit_Information.Units_ID = Unit.Unit_ID")
        cursor.execute(query)
        retrieved = cursor.fetchall()
        cursor.close()
        return retrieved

    def get_all_course_info(self):
        cursor = self.cnx.cursor()
        query = "SELECT Course_Information.Course_Info, Course.Course_Name, Course_Information.Course_Year FROM Course_Information, Course WHERE Course_Information.Course_ID = Course.Course_ID"
        cursor.execute(query)
        retrieved = cursor.fetchall()
        cursor.close()
        return retrieved

    def get_attendees(self, unit_id):
        if unit_id is 0:
            raise Exception('unit_id not set to valid value, is %s' % unit_id)

        cursor = self.cnx.cursor()

        query = (
        "SELECT Student_ID from Unit_assignment JOIN Attendance ON Attendance.Unit_Student = Unit_assignment.Unit_AssignmentID "
        "WHERE Unit_assignment.Unit_ID = %s AND Attendance.Attendended =1 AND DATE(`date`) = DATE(NOW())")

        unit_id = [unit_id]

        cursor.execute(query, unit_id)
        retrieved = cursor.fetchall()
        cursor.close()
        return retrieved

    def create_course_unit_link(self, course_id, unit_id):
        cursor = self.cnx.cursor()

        query = "INSERT INTO Course_Unit(Course_ID, Unit_ID) VALUES (%s, %s)"
        data = [course_id, unit_id]

        cursor.execute(query, data)
        self.cnx.commit()
        return cursor.lastrowid

    def get_sessions_for_course_information(self, course_information_id):
        if course_information_id is 0:
            raise Exception('No course info id given')

        cursor = self.cnx.cursor()

        # Get Course_Information linked with student
        query = 'SELECT * FROM Course_Information WHERE Course_Information.Course_Info = %s'
        data = [course_information_id]
        cursor.execute(query, data)
        course = cursor.fetchone()
        course_info_id = course[0]

        # Get all Course_Units linked with Course_Information
        query = 'SELECT Course_Unit.Unit_ID FROM Course_Unit WHERE Course_Unit.Course_ID = %s'
        data = [course_info_id]
        cursor.execute(query, data)
        links = cursor.fetchall()
        links = [unit_id[0] for unit_id in links]

        # Get all unit information (sessions)
        format_strings = ','.join(['%s'] * len(links))

        query = 'SELECT * FROM Unit_Information where Unit_Information.Units_ID in (%s)'
        query %= format_strings

        cursor.execute(query, links)

        return cursor.fetchall()
