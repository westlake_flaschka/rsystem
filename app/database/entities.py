# All database objects are defined in this file
class Account:
    accountID = 0
    accountEmail = ''  # Email used to login
    accountPassword = ''  # Bcrypt hash of password
    accountPermission = ''
    Stud_ID = 0
    Lect_ID = 0

    def __init__(self, id, email, password, permission, stud_id=0, lect_id=0):
        self.accountID = id
        self.accountEmail = email
        self.accountPassword = password
        self.accountPermission = permission
        self.Stud_ID = stud_id if stud_id is not None else 0
        self.Lect_ID = lect_id if lect_id is not None else 0

    # Required for LoginManager
    @property
    def is_authenticated(self):
        return True

    # Required for LoginManager
    @property
    def is_active(self):
        return True

    # Required for LoginManager
    @property
    def is_anonymous(self):
        return False

    # Required for LoginManager
    def get_id(self):
        return str(self.accountID)


class Lecturer:
    Lect_ID = 0
    Fname = ''
    Lname = ''
    Phone = ''
    Dept = ''
    Email = ''

    def __init__(self, id, fname, lname, phone, dept, email):
        self.Lect_ID = id
        self.Fname = fname
        self.LName = lname
        self.Phone = phone
        self.Dept = dept
        self.Email = email


class Student:
    Stud_ID = 0
    Fname = ''
    Lname = ''
    Phone = ''
    Tutor = 0
    Course = 0
    Email = ''

    def __init__(self, id, fname, lname, phone, tutor, course, email):
        self.Stud_ID = id
        self.Fname = fname
        self.Lname = lname
        self.Phone = phone
        self.Tutor = tutor
        self.Course = course
        self.Email = email


class UnitInfo:
    Unit_ID = 0
    Unit_Room = ''
    Unit_Weekday = ''
    Unit_StartTime = ''
    Unit_FinishTime = ''
    Unit_Lecturer = 0
    Units_ID = 0

    def __init__(self, unitID, room, weekday, startTime, finishTime, lecturer, Units_ID):
        self.Unit_ID = unitID
        self.Unit_Room = room
        self.Unit_Weekday = weekday
        self.Unit_StartTime = startTime
        self.Unit_FinishTime = finishTime
        self.Unit_Lecturer = lecturer
        self.Units_ID = Units_ID
