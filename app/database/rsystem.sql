SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE Account
  (accountID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    accountEmail varchar(45) UNIQUE NOT NULL,
    accountPassword varchar(255) NOT NULL,
    accountPermission char(1) NOT NULL,
    Stud_ID INT NULL,
    Lect_ID INT NULL,
    CONSTRAINT Stud_FK FOREIGN KEY (Stud_ID) REFERENCES Student(Stud_ID),
    CONSTRAINT Lect_FK FOREIGN KEY (Lect_ID) REFERENCES Lecturer(Lect_ID));

CREATE TABLE Lecturer
(Lect_ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
 Fname varchar(20) NOT NULL,
 Lname varchar(20) NOT NULL,
 Phone varchar (15) NOT NULL,
 Dept varchar(45) NOT NULL);

CREATE TABLE Course
    (Course_ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
     Course_Name varchar(30) NOT NULL);


CREATE TABLE Course_Information
    (Course_Info INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
     Course_Year INT NOT NULL,
     Course_ID INT NOT NULL,
     CONSTRAINT CourseInfoFK FOREIGN KEY (Course_ID) REFERENCES Course(Course_ID));

CREATE TABLE Student
    (Stud_ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
     Fname varchar(20) NOT NULL,
     Lname varchar(20) NOT NULL,
     Phone varchar(15) NOT NULL,
     Tutor int NOT NULL,
     Course_Year int NOT NULL,
     CONSTRAINT CourseFK FOREIGN KEY (Course_Year) REFERENCES Course_Information(Course_Info),
     CONSTRAINT TutorFK FOREIGN KEY (Tutor) REFERENCES Lecturer(Lect_ID));

CREATE TABLE Unit
    (Unit_ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
     Unit_Name varchar(20) NOT NULL,
     Unit_Credit int NOT NULL,
     Unit_Coord int NOT NULL,
     CONSTRAINT Coordinator FOREIGN KEY (Unit_Coord) REFERENCES Lecturer(Lect_ID));

CREATE TABLE Unit_Information
    (Unit_ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
     Unit_Room varchar(10) NOT NULL,
     Unit_Weekday INT NOT NULL,
     Unit_StartTime TIME NOT NULL,
     Unit_FinishTime TIME NOT NULL,
     Unit_Lecturer int NOT NULL,
     Units_ID int NOT NULL,
     CONSTRAINT UnitInfo FOREIGN KEY (Units_ID) REFERENCES Unit(Unit_ID),
     CONSTRAINT UnitLect FOREIGN KEY (Unit_Lecturer) REFERENCES Lecturer (Lect_ID));


CREATE TABLE Course_Unit
    (Course_ID int NOT NULL,
     Unit_ID int NOT NULL,
     CONSTRAINT CourseUnit PRIMARY KEY (Course_ID, Unit_ID),
     CONSTRAINT CourseID FOREIGN KEY (Course_ID) REFERENCES Course_Information(Course_Info),
     UNIQUE INDEX (Unit_ID, Course_ID),
     CONSTRAINT UnitID FOREIGN KEY (Unit_ID) REFERENCES Unit_Information(Unit_ID));

CREATE TABLE Unit_Assignment
    (Unit_AssignmentID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Unit_ID int NOT NULL,
    Student_ID int NOT NULL,
    CONSTRAINT IDCourse FOREIGN KEY (Unit_ID) REFERENCES Unit_Information(Unit_ID),
    CONSTRAINT IDStudent FOREIGN KEY (Student_ID) REFERENCES Student(Stud_ID));

CREATE TABLE Attendance
    (AttendanceID int PRIMARY KEY NOT NULL,
     Unit_Student INT NOT NULL,
     Attendended BOOL NOT NULL,
     date DATETIME NOT NULL,
    CONSTRAINT UNITSTUDENT FOREIGN KEY(Unit_Student) REFERENCES Unit_Assignment (Unit_AssignmentID));

SET FOREIGN_KEY_CHECKS = 1;