#RSystem#

##Installation##
RSystem uses Python3.4 and onwards. Ensure that you have Python installed and check the version on Windows using the command:

```
python --version
```

Then run the appropriate installation script for your OS which will download the libraries neccessary to run RSystem. Then run either execute 'run.py' on linux or 'run_windows.bat' to start the web-server, once it has finished navigate to the indicated address to view the website.

##Database##
RSystem uses MySQL. Run the script in rsystem.sql to create the database. You will need to supply a mysql.cnf file for connecting to the database.

##Web Framework##
RSystem uses the Flask Python library to render and serve webpages, as well as manage user sessions.

##About##
A system designed to track attendance in an education environment.