$venvName = "flask"

if (Test-Path $venvName)
{
    Write-Host "Venv $venvName already exists, removing" -ForegroundColor Yellow
    Remove-Item -Recurse -Force $venvName
}

$python = (python --version).Split(' ')

if ($python.Length -eq 2 -and $python[0] -eq "Python")
{
    Write-Host "Detected Python version as: $python" -ForegroundColor Green
    Write-Host ""
}
else
{

    Write-Host "Could not detect Python, please install it manually" -ForegroundColor Red
    exit
}

Write-Host "Updating your systems version of pip" -ForegroundColor Green
python -m pip install -U pip

Write-Host "Fetching virtualenv from pip" -ForegroundColor Green
pip install virtualenv

Write-Host "Creating virtualenv under ./$venvName" -ForegroundColor Green
python -m venv $venvName

Write-Host "Updating venv version of pip" -ForegroundColor Green
.\flask\Scripts\python.exe -m pip install --upgrade pip

Write-Host "Installing requirements for rsystem" -ForegroundColor Green
.\flask\Scripts\pip.exe install -r requirements.txt

$url = "https://dev.mysql.com/get/Downloads/Connector-Python/mysql-connector-python-2.1.5.zip"
$output = $env:tmp + "\mysqlconnector.zip"

if (Test-Path $output)
{
    Write-Host "Mysql connector seems to already exist at $output" -ForegroundColor Yellow
}
else
{
    Write-Host "Now downloading mysql connector for python from Oracle" -ForegroundColor Yellow
    (New-Object System.Net.WebClient).DownloadFile($url, $output)
}

$destination = $env:tmp + "\sqlconnector"

if (Test-Path $destination)
{
    Remove-Item -Recurse -Force $destination
}

Add-Type -assembly "system.io.compression.filesystem"

Write-Host "Extracting zip"
[io.compression.zipfile]::ExtractToDirectory($output, $destination)

Write-Host "Activating venv in $venvName"
iex ".\$venvName\Scripts\activate.ps1"

Write-Host "Installing mysqlconnector to venv"
cd $destination\mysql-connector-python-2.1.5
iex "python setup.py install"

Write-Host "Cleaning up files and folders"
Remove-Item -Recurse -Force $destination
Remove-Item -Force $output

Write-Host "Installation complete" -ForegroundColor Yellow
Write-Host "Now setup MySQL and create mysql.cnf file" -ForegroundColor Yellow
